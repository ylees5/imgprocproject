# -*- coding: utf-8 -*-
import sys
import os
import numpy as np
import cv2
import platform


def onHTrackChange(m):
    pass
def onSTrackChange(m):
    pass
def onVTrackChange(m):
    pass
def onBallTrackChange(m):
    pass

def eatArgs():
    """Eat cmd args, return filename"""
    if len(sys.argv) <= 1:
        sys.stderr.write(u'Usage: %s <video file> \n' % sys.argv[0])
        sys.exit(1)
    print "working with viedo %s" % (sys.argv[1])
    return sys.argv[1]

def createGUI(video):
    """Creates all the windows needed for the program."""
    cv2.namedWindow("Video", cv2.WINDOW_NORMAL)
    #cv2.namedWindow("Lanes", cv2.WINDOW_NORMAL)
    cv2.namedWindow("Balls", cv2.WINDOW_NORMAL)
    cv2.namedWindow("Controls", cv2.WINDOW_NORMAL)
    #DOTO võiks lugeda/kirjutada need parameetrid mingisse conf faili
    cv2.createTrackbar('H_max', 'Controls', 26, 255, onHTrackChange)
    cv2.createTrackbar('H_min', 'Controls', 0, 255, onHTrackChange)
    cv2.createTrackbar('S_max', 'Controls', 159, 255, onSTrackChange)
    cv2.createTrackbar('S_min', 'Controls', 54, 255, onSTrackChange)
    cv2.createTrackbar('V_max', 'Controls', 255, 255, onVTrackChange)
    cv2.createTrackbar('V_min', 'Controls', 105, 255, onVTrackChange)
    #ball detection sliders
    cv2.createTrackbar('Ball_max', 'Controls', 215, 255, onBallTrackChange)
    cv2.createTrackbar('Ball_min', 'Controls', 20, 255, onBallTrackChange)
    
    #resize windows
    f_width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
    f_height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
    #cv2.resizeWindow("Video", f_width/2, f_height/2)
    #cv2.resizeWindow("Lanes", f_width/2, f_height/2)
    cv2.resizeWindow("Balls", f_width/2, f_height/2)
    
def videoOpenTest(video):
    """Test if the videofile is opend. Exit the program if it is not."""
    if not video.isOpened():
        print 'ERROR: Could not open the video file!'
        if platform.system() == 'Windows':
            print 'Try adding opencv_ffmpegXXX.dll to the Windows PATH environment variable.'
        sys.exit()
    else:
        print 'Video opening successful.'
        
def filterLaneColors(frame, kernel):
    """Converts the frame to HSV color space and filters the colors. After that some
    morphologycal operation are applied on the binary image to remove noise."""
    #color filtering
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    lower_boundary = np.array([cv2.getTrackbarPos('H_min', 'Controls'),
                              cv2.getTrackbarPos('S_min', 'Controls'),
                              cv2.getTrackbarPos('V_min', 'Controls')])
    upper_boundary = np.array([cv2.getTrackbarPos('H_max', 'Controls'),
                              cv2.getTrackbarPos('S_max', 'Controls'),
                              cv2.getTrackbarPos('V_max', 'Controls')])
    mask = cv2.inRange(hsv, lower_boundary, upper_boundary)
    
    
    mask = cv2.erode(mask,kernel,iterations = 2)
    mask = cv2.dilate(mask,kernel,iterations = 3)
    #mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
    return mask
    

    
def findLaneContours(frame, kernel):
    """Use Find Contours to fidnd the lanes. Filter them by area."""
    mask = filterLaneColors(frame, kernel)
    
    _, contours, hierarchy = cv2.findContours(mask, cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)
    #print contours
    newFrame = []
    laneContours = []
    for cnt in contours:
        if 5000 < cv2.contourArea(cnt):
            laneContours.append(cnt)
            cv2.drawContours(frame,[cnt],0,(0,255,0),3)
            #cv2.drawContours(mask,contours, -1, (0,255,0), 3)
            
    return frame, laneContours
    

