def findLaneEdges(frame, line, setupMode = False):
    y, x = frame.shape
    pixel0 = frame[line, 0]
    laneEdges = []
    for i in range(1, x):
        pixel1 = frame[line, i]
        if pixel0 != pixel1:
            laneEdges.append((i,line))
        pixel0 = pixel1
    if setupMode:
        for edge in laneEdges:
            cv2.circle(frame, edge, 10, (0,255,255),4)
    return laneEdges
    
def findLaneBlobs(frame, kernel, detector):
    """Tries to find the
    lanes from the binary image using simple blob detector.
    
    Currently does not work"""
    
    mask = filterLaneColors(frame, kernel)
    
    keypoints = detector.detect(mask)
    print keypoints
    #filtered = cv2.bitwise_and(frame, frame, mask = mask)
    mask = cv2.drawKeypoints(mask, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    
    return mask
    
def makeBlobDetector():
    """Creates the Simple Blob Detector for detecting the bowling lanes.
    Also, here is the place to modify the blob detector parameters"""
    params = cv2.SimpleBlobDetector_Params()
    # Change thresholds
    params.minThreshold = 20;
    params.maxThreshold = 255;
       
    # Filter by Area.
    params.filterByArea = True
    params.minArea = 1000
       
    # Filter by Circularity
    params.filterByCircularity = False
    params.minCircularity = 0.1
        
    # Filter by Convexity
    params.filterByConvexity = False
    params.minConvexity = 0.87
        
    # Filter by Inertia
    params.filterByInertia = False
    params.minInertiaRatio = 0.01
        
    # Create a detector with the parameters
    ver = (cv2.__version__).split('.')
    if int(ver[0]) < 3 :
        return cv2.SimpleBlobDetector(params)
    else : 
        return cv2.SimpleBlobDetector_create(params)
        
def shiftRefFrame(buffer, frame):
    buffer.append(frame)
    buffer = buffer[1:]
    newReferenc = buffer[0]
    return buffer, newReferenc

def removeGhost(balls, buffer, referenceFrame):
    for ball in balls:
        ((x, y), radius) = ball
        ballBox = referenceFrame[x-radius:x+radius, y-radius:y+radius]
        newRef = buffer[-1]
        newRef[x-radius:x+radius, y-radius:y+radius] = ballBox
        buffer = buffer[:-1] + [newRef]
    return buffer