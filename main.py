# -*- coding: utf-8 -*-
import sys
import os
import numpy as np
import cv2
import platform
import time
from bowlingfunctions import *
from balldetection import *

#initialize 
video = cv2.VideoCapture(eatArgs())        
videoOpenTest(video)    
createGUI(video)

lane_kernel = np.ones((4,4),np.uint8)
ball_kernel = np.ones((19,19),np.uint8)
#lane_kernel = cv2.getStructuringElement(cv2.MORPH_CROSS,(5,5))

#get referents frame
video.set(cv2.CAP_PROP_POS_MSEC,3900)
_, referenceFrame = video.read()
_, refCpy = video.read()
_, backupReference = video.read()
video.set(cv2.CAP_PROP_POS_MSEC,0)

laneImg, laneContours = findLaneContours(referenceFrame,lane_kernel)
cv2.imwrite('Detected_lanes.png', laneImg)

  
noRef = 0
frameCount = 0
timeAcum = 0
  
while(video.isOpened()):
    startTime = time.time()
    _, frame = video.read()
    
    cpy1 = frame
    cpy2 = frame
    #cv2.imshow('Video',frame)
    motion = motionDetection(cpy1, refCpy, ball_kernel)
    ret, balls = detectBallContours(cpy2, motion, laneContours)
    
    #if there is no balls detected on the frame take this frame as new refernce
    if len(balls) == 0:
        refCpy = frame
        noRef = 0
    else:
        #dirty workaround for stuck balls
        noRef += 1
        if noRef > 40:
            refCpy = backupReference
            noRef = 0
    
    cv2.imshow('Balls', motion)
    cv2.imshow('Video', ret)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
        
        
    #measure fps
    endTime = time.time()
    frameTime = endTime - startTime
    timeAcum += frameTime
    frameCount += 1
    if frameCount == 30:
        fps = 30 / timeAcum
        print 'FPS: %.1f' % fps
        frameCount = 0
        timeAcum = 0
        
video.release()
cv2.destroyAllWindows()
