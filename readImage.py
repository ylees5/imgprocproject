import sys
import os
import numpy as np
import cv2

def onMAXTrackChange(m):
    pass
def onMINTrackChange(m):
    pass

def onMouse(e, x, y, u, v):
    if e == cv2.EVENT_LBUTTONDOWN:
        print str(x) + '-' + str(y) + ': ' + str(dilated[y, x])
        

def eat_args():
    # Eat cmd args, return filename what to dump and server where to dump
    if len(sys.argv) <= 1:
        sys.stderr.write(u'Usage: %s <video file> \n' % sys.argv[0])
        sys.exit(1)
    print "working with viedo %s" % (sys.argv[1])
    return sys.argv[1]

def createGUI():
    cv2.namedWindow("image")
    cv2.createTrackbar('MAX', 'image', 200, 255, onMAXTrackChange)
    cv2.createTrackbar('MIN', 'image', 60, 255, onMINTrackChange)
    cv2.setMouseCallback('image', onMouse)

if __name__ == '__main__':

    vidName = 'db/' + eat_args()
    refFrame = vidName + '/ref_before.jpg'

    imfiles = [ f for f in os.listdir(vidName) if os.path.isfile(os.path.join(vidName,f)) ]
    nrOfImages = len(imfiles) - 1
    i = 0
    I = cv2.imread( os.path.join(vidName,imfiles[i]) )
    ref = cv2.imread(refFrame)
    ref = cv2.resize(ref, (1280, 720), interpolation = cv2.INTER_AREA)
    ref = cv2.cvtColor(ref, cv2.COLOR_BGR2GRAY)
    createGUI()

    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(10,10))
    subs = False
    
    while 1:
        I = cv2.resize(I, (1280, 720), interpolation = cv2.INTER_AREA)
        if subs:
            gray = cv2.cvtColor(I, cv2.COLOR_BGR2GRAY)
            tMax = cv2.getTrackbarPos('MAX', 'image')
            tMin = cv2.getTrackbarPos('MIN', 'image')
            substractedImage = gray - ref
            tRet, threshMax = cv2.threshold(substractedImage, tMax, 255, cv2.THRESH_TOZERO_INV)
            tRet, thresh = cv2.threshold(threshMax, tMin, 255, cv2.THRESH_BINARY)
            sthresh = cv2.resize(thresh, (1280, 720), interpolation = cv2.INTER_AREA)
            dilated = cv2.dilate(sthresh, kernel)
            contours, hierarchy = cv2.findContours(dilated, cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
            
            #print len(contours)
            
            #circles = cv2.HoughCircles(thresh,cv2.HOUGH_GRADIENT,1,20,
             #               param1=50,param2=30,minRadius=0,maxRadius=0)
            #print circles
            #if circles != None:
             #   circles = np.uint16(np.around(circles))
                
              #  for i in circles[0,:]:
                    # draw the outer circle
                    #cv2.circle(I,(i[0],i[1]),i[2],(0,255,0),2)
                    # draw the center of the circle
               #     cv2.circle(I,(i[0],i[1]),2,(0,0,255),3)
            
            cv2.drawContours(I,contours, -1, (0,255,0), 3)
            if len(contours) > 0:
                ball = contours[0]
                M = cv2.moments(ball)
                #print M
                ballX = int(M['m10']/M['m00'])
                ballY = int(M['m01']/M['m00'])
                cv2.putText(I, str(ballX) + '|' + str(ballY), (ballX, ballY), 1, 1,(0, 0, 255))
            #cv2.imshow("image", I)
        #else:
            
        cv2.imshow("image", I)    
        k = cv2.waitKey(1)
        if k == 27: #esc
            break
        elif k == 2555904: #right arrow
            i = (i + 1) % nrOfImages # move to next image
            I = cv2.imread( os.path.join(vidName,imfiles[i]) )
        elif k == 2424832: #left arrow
            i = (nrOfImages + i - 1) % nrOfImages # move to previous image
            I = cv2.imread( os.path.join(vidName,imfiles[i]) )
        elif k == 120: #x
            subs = True
        elif k == 122: #z
            subs = False
        elif k <> -1:
            print k
    cv2.destroyAllWindows()
    


