# -*- coding: utf-8 -*-
import sys
import os
import numpy as np
import cv2
import platform
from bowlingfunctions import *

def circleDetect(img):
    circles = cv2.HoughCircles(img,cv2.HOUGH_GRADIENT,1,20,
                            param1=50,param2=30,minRadius=10,maxRadius=100)
    if circles != None:
        circles = np.uint16(np.around(circles))
        for i in circles[0,:]:
            # draw the outer circle
            cv2.circle(img,(i[0],i[1]),i[2],(0,255,0),2)
            # draw the center of the circle
            cv2.circle(img,(i[0],i[1]),2,(0,0,255),3)
    return img

def balldetectLoop(cap):
    while(cap.isOpened()):
        _, frame = cap.read()
        balls = circleDetect(frame)
        cv2.imshow('Balls', balls)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
def motionDetection(frame, refFrame, kernel):
    #convert to grayscale
    f = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
    refF = cv2.cvtColor(refFrame,cv2.COLOR_BGR2GRAY)
    #subtract the reference frame
    motion = f - refF
    #threshold
    tmax = cv2.getTrackbarPos('Ball_max', 'Controls')
    tmin = cv2.getTrackbarPos('Ball_min', 'Controls')
    _, threshMax = cv2.threshold(motion, tmax, 255, cv2.THRESH_TOZERO_INV)
    _, thresh = cv2.threshold(threshMax, tmin, 255, cv2.THRESH_BINARY)
    #morphological closing
    #dilated = cv2.dilate(thresh, kernel)
    #closed = cv2.erode(dilated, kernel)
    
    return thresh

def detectBallContours(frame, img, lanes):
    _,contours, hierarchy = cv2.findContours(img, cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    balls = []
    for cnt in contours:
        if cv2.contourArea(cnt) > 100:
            c, r = cv2.minEnclosingCircle(cnt)
            x = int(c[0])
            y = int(c[1])
            c = (x, y)
            r = int(r)
            if y > 900: continue
            if r < 25:
                for lane in lanes:
                    onLane = cv2.pointPolygonTest(lane, c, True)
                    if onLane > 10:
                        cv2.circle(frame, c, r, (0, 0, 255), 3)
                        balls.append((c, r))
                        break
    return  frame, balls
